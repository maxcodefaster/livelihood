import * as Nano from 'nano';
import { Controller, Inject, OnModuleInit } from '@nestjs/common';
import { chatDesignDocuments } from 'src/core/design-docs/chat-design-docs';

@Controller('chat')
export class ChatController implements OnModuleInit {

    couch: any = Nano('http://' + process.env.COUCHDB_USR + ':' + process.env.COUCHDB_PW + '@' + process.env.COUCHDB_HOST + ':' + process.env.COUCHDB_PORT);

    chatIndex = this.couch.use('chat-index');
    users = this.couch.use('users');

    constructor(
        @Inject('superlogin') private superlogin: any,
    ) { }

    async onModuleInit() {
        await new Promise(resolve => setTimeout(resolve, 1000));
        this.chatIndexListener()
    }

    chatIndexListener() {
        const feed = this.chatIndex.follow({ since: "now", include_docs: true });
        feed.on('change', (change) => {
            if (change.deleted === undefined) {
                this.createConvo(change.doc)
            }
        });
        feed.follow();
        process.nextTick(() => {
        });
    }

    createConvo(convo) {
        // create DB
        this.couch.db.create(convo._id).then((body) => {
            // add DB to shared DBs in UserDocs
            this.addDbToUserDocs(convo);
            // insert view docs
            const chatDb = this.couch.use(convo._id);
            for (let doc of chatDesignDocuments) {
                chatDb.insert(doc).then(
                    result => {
                        // console.log(result);
                    },
                    err => { console.log(err.message); }
                );
            }
        }).catch((err) => { console.log(err) });
    }

    addDbToUserDocs(convo) {
        const dbName = convo._id;
        for (let user of convo.participants) {
            this.superlogin.getUser(user).then((user) => {
                // add user sessions to chatDB security members
                if (user.session != undefined) {
                    for (let session in user.session) {
                        this.addSecurityMembers(session, dbName);
                    }
                };
                // add convo DB to user doc
                user.personalDBs[dbName] = {
                    name: dbName,
                    type: 'shared',
                }
                this.users.insert(user).then(
                    result => {
                        // console.log(result)
                    },
                    err => { console.log(err.message); }
                );
            }).catch((err) => { console.log(err) })
        }
    }

    addSecurityMembers(user, db) {
        const url = '_security'
        const optsGet = {
            db: db,
            path: url
        };
        this.couch.request(optsGet).then((secDoc: any) => {
            secDoc.members.names !== undefined ? secDoc.members.names.push(user) : secDoc.members = { names: [user] };
            const optsPut = {
                db: db,
                path: url,
                method: 'put',
                body: secDoc
            };
            this.couch.request(optsPut).then((res) => {
                // console.log(res);
            }).catch((err) => { console.log(err) });
        }).catch((err) => console.log(err));
    }

}

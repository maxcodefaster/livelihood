import { Injectable, MethodNotAllowedException } from '@nestjs/common';
import * as nano from 'nano';


@Injectable()
export class AuthService {

    couch: any = nano({
        url: 'http://' + process.env.COUCHDB_USR + ':' + process.env.COUCHDB_PW + '@' + process.env.COUCHDB_HOST + ':' + process.env.COUCHDB_PORT
    });

    _users = this.couch.use('_users');

    // return true or false if user is allowed to view user doc
    async checkToken(token, id): Promise<Boolean> {
        token = token.match(/\s(.*):/)[1]
        let allowed: Boolean;
        // get user doc from auth token
        await this._users.view('additionalAuth', 'token_authorized', {
            'key': token,
            'include_docs': true
        }).then((body) => {
            // check if user has admin role
            const user = body.rows[0].doc;
            if (user.user_id === id || user.roles.includes('admin')) {
                allowed = true;
            } else {
                allowed = false;
            }
        }).catch((err) => {
            console.log(err);
            allowed = false;
        });
        return allowed;
    }
}


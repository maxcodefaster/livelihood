export const _usersDesignDocuments = [
    {
        _id: '_design/additionalAuth',
        language: 'javascript',
        views: {
            token_authorized: {
                map: "function (doc) { if (doc.name) { emit(doc.name, doc._id); } }"
            }
        }
    }
]
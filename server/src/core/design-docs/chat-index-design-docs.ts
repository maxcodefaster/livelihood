export const chatIndexDesignDocuments = [
    {
        _id: '_design/chatIndex',
        language: 'javascript',
        type: 'view',
        participants: [],
        views: {
            privateChats: {
                map: "function (doc) { if (doc.type == 'privateChat'){ emit(doc._id, doc.participants) }};"
            }
        },
        filters: {
            byUserId: "function (doc, req) {if((doc.type == 'view') || (doc.participants.indexOf(req.query.userId) != -1)) {return true} else {return false}};"
        }
    }
]
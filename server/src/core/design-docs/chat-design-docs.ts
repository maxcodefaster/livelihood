export const chatDesignDocuments = [
    {
        _id: '_design/chatMsg',
        views: {
            by_date_created: {
                map: "function(doc){ if(doc.type == 'chatMsg'){ emit(doc.dateCreated);} }"
            }
        }
    }
]
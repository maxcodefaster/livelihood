import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { join } from 'path';
import * as bodyParser from 'body-parser';
import { AppModule } from './app.module';
import { SuperloginModule } from './core/superlogin-module';

async function bootstrap() {
  const fs = require('fs');
  const httpsOptions = {
    key: fs.readFileSync('/etc/letsencrypt/live/livelihood.foundation/privkey.pem'),
    cert: fs.readFileSync('/etc/letsencrypt/live/livelihood.foundation/cert.pem'),
  };
  const app = await NestFactory.create<NestExpressApplication>(AppModule, {
    httpsOptions
  });
  app.enableCors();
  app.useStaticAssets(join(__dirname, '.', 'superlogin/email-templates'));
  SuperloginModule.setup('/auth', app);
  app.use(bodyParser.json({ limit: '50mb' }));
  app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
  await app.listen(3333);
}
bootstrap();

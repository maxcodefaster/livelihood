import { Module, Inject } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

import { dbSetup } from './core/db-setup';
import { superloginConfig } from './core/superlogin-config';
import { SuperloginModule } from './core/superlogin-module';
import { signupHandler } from './core/signup-handler';
import { UserModule } from './user/user.module';
import { StreamService } from './core/services/stream/stream.service';
import { AuthService } from './core/services/auth/auth.service';
import { ChatModule } from './chat/chat.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    SuperloginModule.forRoot(superloginConfig),
    UserModule,
    ChatModule,
  ],
  controllers: [],
  providers: [StreamService, AuthService],
})

export class AppModule {
  constructor(@Inject('superlogin') private superlogin: any) {
    this.superlogin.on('signup', signupHandler);
    dbSetup();
  }

}
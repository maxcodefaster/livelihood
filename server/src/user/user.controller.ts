import * as Nano from 'nano';
import { Controller, Get, Inject, Param, Post, Body, Res, UseInterceptors, UploadedFile, Req } from '@nestjs/common';
import { StreamService } from '../core/services/stream/stream.service';
import { Response, Request } from 'express';
import { FileInterceptor } from '@nestjs/platform-express';
import { AuthService } from 'src/core/services/auth/auth.service';

@Controller('user')
export class UserController {

    couch: any = Nano('http://' + process.env.COUCHDB_USR + ':' + process.env.COUCHDB_PW + '@' + process.env.COUCHDB_HOST + ':' + process.env.COUCHDB_PORT);

    users = this.couch.use('users');

    constructor(
        @Inject('superlogin') private superlogin: any,
        private streamService: StreamService,
        private authService: AuthService,
    ) { }


    @Get('profile/:id')
    async getProfile(@Param() id, @Res() res: Response) {
        this.superlogin.getUser(id.id).then((user) => {
            res.send({ bio: user.bio, _attachments: user._attachments })
        }).catch((err) => { console.log(err) })
    }

    @Get('info/:id')
    async getUserDoc(@Req() request: Request, @Param() id) {
        if (await this.authService.checkToken(request.headers.authorization, id.id)) {
            return this.superlogin.getUser(id.id);
        }
    }

    @Post('profile-img')
    @UseInterceptors(FileInterceptor('file'))
    async editUserProfile(@Req() request: Request, @Body() newDetails, @UploadedFile() file, @Res() res: Response) {
        if (await this.authService.checkToken(request.headers.authorization, newDetails.id)) {
            await this.users.get(newDetails.id).then(async (body) => {
                let userDoc: any = body;
                // if user has supplied a profile pic start inserting
                if (file) {
                    // if user has an attachment
                    if (userDoc._attachments) {
                        // get name of attachment
                        const profileImgKey = Object.keys(userDoc._attachments)[0];
                        //destroy it
                        await this.users.attachment.destroy(userDoc._id, profileImgKey, { rev: userDoc._rev }).then(async () => {
                            // get updated user doc (new _rev)
                            await this.users.get(userDoc._id).then((body) => {
                                userDoc = body;
                            }).catch((err) => { res.send(err) });
                        }).catch((err) => { res.send(err); });
                    }
                    // finally insert profile pic
                    await this.users.attachment.insert(userDoc._id, file.originalname, file.buffer, file.mimetype, { rev: userDoc._rev }).then(async (body) => {
                        // if user also supplied new bio
                        if (newDetails.bio) {
                            // get updated user doc (new _rev)
                            await this.users.get(userDoc._id).then((body) => {
                                userDoc = body;
                            }).catch((err) => { res.send(err) });
                        } else { res.send(body) }
                    }).catch((err) => { res.send(err); });
                }
                if (newDetails.bio) {
                    // if user has supplied bio
                    userDoc.bio = newDetails.bio;
                    // insert bio
                    this.users.insert(userDoc).then(
                        result => { res.send(result); },
                        err => { res.send(err.message); }
                    );
                }
            }).catch((err) => {
                res.send(err);
            });
        } else {
            return res.status(403).json({ status: false, message: 'Wrong authentication token' });
        }
    }

    @Get('profile-img/:id')
    async getUserProfileImg(@Param() id, @Res() res: Response) {
        let user;
        await this.superlogin.getUser(id.id).then(userDoc => {
            user = userDoc;
        })
        // if user has attachment
        if (user._attachments) {
            // get attachment name and values
            const profileImgKey = Object.keys(user._attachments)[0];
            const profileImgValues: any = Object.values(user._attachments)[0];
            // retrieve the blob and pipe it back to client
            this.users.attachment.get(id.id, profileImgKey).then((buffer) => {
                const stream = this.streamService.getReadableStream(buffer);
                res.set({
                    'Content-Type': profileImgValues.content_type,
                    'Content-Transfer-Encoding': 'binary',
                    'Content-Length': buffer.length,
                });
                stream.pipe(res);
            });
        } else {
            res.set({
                'Content-Type': 'application/json',
            });
            return res.status(404).json({ status: false, message: 'No profile img.' });
        }
    }

    @Get('delete/:id')
    deleteUser(@Param() id) {
        return this.superlogin.removeUser(id.id, true);
    }

    @Get('admin/list')
    async listUser() {
        let users = [];
        await this.users.view('userDoc', 'all_users', {
            'include_docs': true
        }).then((body) => {
            body.rows.forEach((doc) => {
                users.push(doc.doc);
            })
        }).catch((err) => {
            console.log(err);
            return err;
        });
        return users;
    }

    @Post('admin/edit')
    editUser(@Body() newDetails, @Res() res: Response) {
        this.users.get(newDetails.user_id).then((body) => {
            let userDoc: any = body;
            // set new user role
            userDoc.roles.splice(0, 1, newDetails.role);
            userDoc.email = newDetails.email;
            this.users.insert(userDoc).then(
                result => { res.send(result); },
                err => { res.send(err.message); }
            );
        }).catch((err) => { res.send(err); });
    }

}

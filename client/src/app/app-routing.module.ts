import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'info', pathMatch: 'full' },
  { path: 'info', loadChildren: './info/info.module#InfoPageModule' },
  { path: 'app', loadChildren: './home/home.module#HomePageModule' },
  { path: 'notes', loadChildren: './notes/notes.module#NotesPageModule' },
  { path: 'login', loadChildren: './auth-gateway/login/login.module#LoginPageModule' },
  { path: 'register', loadChildren: './auth-gateway/register/register.module#RegisterPageModule' },
  { path: 'forgot-pwd', loadChildren: './auth-gateway/forgot-pwd/forgot-pwd.module#ForgotPwdPageModule' },
  { path: 'reset-pwd/:token', loadChildren: './auth-gateway/reset-pwd/reset-pwd.module#ResetPwdPageModule' },
  { path: 'profile/:id', loadChildren: './profile/user/profile.module#ProfilePageModule' },
  { path: 'settings', loadChildren: './user-settings/settings.module#SettingsPageModule' },
  { path: 'chat/:id', loadChildren: './chat/chat/chat.module#ChatPageModule' },
  { path: 'utility', loadChildren: './chat/utility/utility.module#UtilityPageModule' },
  { path: 'ride', loadChildren: './chat/ride/ride.module#RidePageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

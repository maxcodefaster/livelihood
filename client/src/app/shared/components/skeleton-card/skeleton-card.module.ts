import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { SkeletonCardComponent } from './skeleton-card.component';

@NgModule({
  declarations: [SkeletonCardComponent],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [SkeletonCardComponent]
})
export class SkeletonCardModule { }

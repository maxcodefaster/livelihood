import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SharedDocService } from 'src/app/services/shared-doc.service';
import { AlertController, ActionSheetController, ModalController, ToastController, NavController } from '@ionic/angular';
import { SearchFilterService } from '../../helpers/search-filter.service';
import { FormControl } from '@angular/forms';
import { EditDocModal } from '../../modals/edit-doc/edit-doc.modal';
import { debounceTime } from 'rxjs/operators';
import { UserService } from 'src/app/core/user.service';
import { PrivateDocService } from 'src/app/services/private-doc.service';

@Component({
  selector: 'app-doc-list',
  templateUrl: './doc-list.component.html',
  styleUrls: ['./doc-list.component.scss'],
})
export class DocListComponent implements OnInit {

  @Input('doc') docs: any = [];
  @Output() docUpdated = new EventEmitter<boolean>();
  currentUser;
  service: string;
  searching: boolean = false;
  refreshing: boolean = false;
  searchControl: FormControl;

  constructor(
    private sharedDocService: SharedDocService,
    private privateDocService: PrivateDocService,
    private navCtrl: NavController,
    private searchFilterService: SearchFilterService,
    public alertController: AlertController,
    public actionSheetController: ActionSheetController,
    private userService: UserService,
    private modalController: ModalController,
    public toastCtrl: ToastController
  ) {
    this.searchControl = new FormControl();
  }

  ngOnInit() {
    this.currentUser = this.userService.currentUser;
    this.initializeSearchForm()
    this.docs[0].type === 'privateDoc' ? this.service = 'private' : this.service = 'shared';
  }

  // search form
  initializeSearchForm() {
    this.searchFilterService.getItems(this.docs);
    this.setFilteredItems("");
    this.searchControl.valueChanges
      .pipe(debounceTime(500))
      .subscribe(search => {
        this.searching = false;
        this.setFilteredItems(search);
      });
  }

  setFilteredItems(searchTerm) {
    this.docs = this.searchFilterService.filterItems(searchTerm);
  }

  onSearchInput() {
    this.searching = true;
  }

  docUpdate(updated: boolean) {
    this.docUpdated.emit(updated);
  }

  async openModal(doc?) {
    const modal = await this.modalController.create({
      component: EditDocModal,
      swipeToClose: true,
      componentProps: {
        'doc': doc,
        'db': this.service,
      }
    }).then((modal) => {
      modal.present();
      modal.onWillDismiss().then(() => {
        this.docUpdate(true);
      });
    });
  }


  async presentActionSheet(doc) {
    let canEdit, canChat;
    doc.author === this.currentUser.user_id || this.currentUser.roles[0] === 'admin' ? canEdit = '' : canEdit = 'actionSheetButtonInvisible';
    doc.author === this.currentUser.user_id ? canChat = 'actionSheetButtonInvisible' : canChat = '';
    const actionSheet = await this.actionSheetController.create({
      translucent: true,
      header: doc.title + ' - by ' + doc.author,
      buttons: [
        {
          text: 'Chat',
          cssClass: canChat,
          icon: 'chatbubbles-outline',
          handler: () => {
            this.navCtrl.navigateRoot('/chat/' + doc.author);
          }
        }, {
          text: 'Edit',
          cssClass: canEdit,
          icon: 'pencil',
          handler: () => {
            this.openModal(doc);
          }
        }, {
          text: 'Delete',
          role: 'destructive',
          cssClass: canEdit,
          icon: 'trash',
          handler: () => {
            this.presentDeleteConfirm(doc);
          }
        }, {
          text: 'Cancel',
          icon: 'close',
          role: 'cancel',
          handler: () => {
          }
        }]
    });
    await actionSheet.present();
  }

  async presentDeleteConfirm(doc) {
    const alert = await this.alertController.create({
      translucent: true,
      header: 'Delete',
      message: 'Do you want to delete <strong>' + doc.title + '</strong> ?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
        }, {
          text: 'Okay',
          handler: () => {
            this.deleteDoc(doc);
          }
        }
      ]
    });
    await alert.present();
  }

  async deleteDoc(doc) {
    this.service === 'shared' ? await this.sharedDocService.deleteSharedDoc(doc) : await this.privateDocService.deletePrivateDoc(doc);
    this.docUpdate(true);
  }

  async docDeletedToast() {
    const toast = await this.toastCtrl.create({
      message: 'Successfully deleted',
      translucent: true,
      duration: 2000
    });
    toast.present();
  }

}

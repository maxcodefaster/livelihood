import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ErrorMessagesComponent } from './error-messages.component';
import { IonicModule } from '@ionic/angular';



@NgModule({
  declarations: [ErrorMessagesComponent],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [ErrorMessagesComponent]
})
export class ErrorMessagesModule { }

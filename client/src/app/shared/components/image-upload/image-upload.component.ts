import { Component, OnInit, ViewChild, ElementRef, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ActionSheetController, Platform } from '@ionic/angular';
import { ImageService } from '../../services/image.service';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { ImgInterface } from './img-interface';

@Component({
  selector: 'app-image-upload',
  templateUrl: './image-upload.component.html',
  styleUrls: ['./image-upload.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ImageUploadComponent),
      multi: true
    }
  ]
})
export class ImageUploadComponent implements OnInit, ControlValueAccessor {

  @Input('img') img: ImgInterface;
  @Input() disabled = false;
  @ViewChild('browserphoto') pwaphoto: ElementRef;

  platformIs: string;
  ratio;
  maintainAspectRatio: boolean;
  roundCropper;
  imgSource: Blob;
  imgBlob: Blob;
  showCameraPicker: boolean = true;

  constructor(
    private actionSheetController: ActionSheetController,
    public imageService: ImageService,
    public platform: Platform
  ) {
    if ((this.platform.is('android') || this.platform.is('ios')) && !this.platform.is('hybrid')) {
      this.showCameraPicker = false;
    }
    if (!this.platform.is('hybrid')) {
      this.platformIs = 'browser';
    }
  }

  ngOnInit() {
    this.imgBlob = new Blob([this.img.blob], { type: 'image/png' });
    this.imgSource = new Blob([this.img.blob], { type: 'image/png' });
    this.roundCropper = this.img.roundCropper;
    this.maintainAspectRatio = this.img.maintainAspectRation;
    if (this.img.ratio) this.ratio = this.img.ratio;
  }

  // ControlValueAccessor interface
  onChange: any = () => { }

  onTouch: any = () => { }

  writeValue(obj: any): void { }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  // ngx-image-cropper
  imageCropped(event: ImageCroppedEvent) {
    this.imgBlob = this.base64ToBlob(event.base64);
    this.onChange(this.imgBlob)
  }

  imageLoaded() {
    // show cropper
  }
  cropperReady() {
    // cropper ready
  }
  loadImageFailed() {
    // show message
  }

  openPWAPhotoPicker() {
    if (this.pwaphoto == null) {
      return;
    }
    this.pwaphoto.nativeElement.click();
  }

  parseImage(mode: string): void {
    switch (mode) {
      // Handle image requests via the device camera
      case "camera":
        this.imageService
          .takePicture()
          .then((data) => {
            this.imgBlob = this.base64ToBlob(data.dataUrl);
            this.imgSource = this.base64ToBlob(data.dataUrl);
            this.onChange(this.imgBlob)
          })
          .catch((error) => { console.dir(error); });
        break;
      // Handle image requests via the device photolibrary
      case "library":
        if (this.platformIs === 'browser') {
          if (this.pwaphoto == null) {
            return;
          }
          this.pwaphoto.nativeElement.click();
        } else {
          this.imageService
            .selectPhoto()
            .then((data) => {
              this.imgBlob = this.base64ToBlob(data.dataUrl);
              this.imgSource = this.base64ToBlob(data.dataUrl);
              this.onChange(this.imgBlob)
            })
            .catch((error) => { console.dir(error); });
        }
        break;
    }
  }

  chooseImgBrowser() {
    if (this.pwaphoto == null) {
      return;
    }
    const fileList: FileList = this.pwaphoto.nativeElement.files;
    if (fileList && fileList.length > 0) {
      this.fileToBase64(fileList[0]).then((result: string) => {
        this.imgBlob = this.base64ToBlob(result);
        this.imgSource = this.base64ToBlob(result);
        this.onChange(this.imgBlob)
      }, (err: any) => {
        // Ignore error, do nothing
        this.imgBlob = null;
      });
    }
  }

  fileToBase64(fileImage: File): Promise<{}> {
    return new Promise((resolve, reject) => {
      let fileReader: FileReader = new FileReader();
      if (fileReader && fileImage != null) {
        fileReader.readAsDataURL(fileImage);
        fileReader.onload = () => {
          resolve(fileReader.result);
        };
        fileReader.onerror = (error) => {
          reject(error);
        };
      } else {
        reject(new Error('No file found'));
      }
    });
  }

  base64ToBlob(dataURI: string) {
    const byteString = atob(dataURI.split(',')[1]);
    const fileType = (dataURI.split(';')[0]).split(':')[1];
    const ab = new ArrayBuffer(byteString.length);
    let ia = new Uint8Array(ab);

    for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ab], { type: fileType });
  }

  async launchActionSheet() {
    let mobilePicker;
    this.showCameraPicker ? mobilePicker = '' : mobilePicker = 'actionSheetButtonInvisible';
    const actionSheet = await this.actionSheetController.create({
      header: "Select Image source",
      translucent: true,
      buttons: [{
        text: 'Choose Image',
        handler: () => {
          this.parseImage('library');
        }
      },
      {
        text: 'Use Camera',
        cssClass: mobilePicker,
        handler: () => {
          this.parseImage('camera');
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }

}

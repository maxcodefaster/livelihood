import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from 'src/app/core/auth.service';
import { UserService } from 'src/app/core/user.service';
import { IonSlides, Platform } from '@ionic/angular';

@Component({
  selector: 'info-slider',
  templateUrl: './info-slider.component.html',
  styleUrls: ['./info-slider.component.scss'],
})
export class InfoSliderComponent implements OnInit {

  @ViewChild('slides') slides: IonSlides;

  currentUser;
  isMobile = false;

  constructor(
    private authService: AuthService,
    private userService: UserService,
    public platform: Platform,
  ) { }

  ngOnInit() {
    if (this.platform.is('mobile') || this.platform.is('android')) {
      this.isMobile = true;
    }
    this.authService.reauthenticate().then((res) => {
      this.currentUser = this.userService.currentUser;
    }, (err) => {
    });
  }

  next() {
    this.slides.slideNext();
  }

  prev() {
    this.slides.slidePrev();
  }

}

import { Injectable } from '@angular/core';
import { Plugins, CameraResultType, CameraSource } from '@capacitor/core';
import { SafeResourceUrl } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const { Camera } = Plugins;

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  private fileReader: any = new FileReader();
  private image: SafeResourceUrl;

  constructor(
    public http: HttpClient,
  ) { }

  async takePicture(): Promise<any> {
    /* Define the options for the getPhoto method - particularly the source for where 
       the image will be taken from (I.e. the device camera) and how we want the captured 
       image data returned (I.e. base64 string or a file uri) */
    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: true,
      resultType: CameraResultType.DataUrl,
      source: CameraSource.Camera
    });
    /* We need to run the returned Image URL through Angular's DomSanitizer to 'trust' 
       this for use within the application (I.e. so that Angular knows this isn't an 
       XSS attempt or similarly malicious code) */
    // this.image = this.sanitizer.bypassSecurityTrustResourceUrl(image && (image.webPath));
    this.image = image;
    return this.image;
  }

  async selectPhoto(): Promise<any> {
    /* Define the options for the getPhoto method - particularly how we want the
       image data returned (I.e. base64 string or a file uri) */
    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: false,
      resultType: CameraResultType.DataUrl
    });
    // We return the webPath property of the image object (which contains the image path)
    return image.webPath;
  }

  selectImage(event): Observable<any> {
    return Observable.create((observer) => {
      this.handleImageSelection(event)
        .subscribe((res) => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  handleImageSelection(event: any): Observable<any> {
    let file: any = event.target.files[0];
    this.fileReader.readAsDataURL(file);
    return Observable.create((observer) => {
      this.fileReader.onloadend = () => {
        observer.next(this.fileReader.result);
        observer.complete();
      }
    });
  }
}
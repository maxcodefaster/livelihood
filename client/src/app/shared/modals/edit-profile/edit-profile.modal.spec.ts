import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditProfileModal } from './edit-profile.modal';

describe('EditProfileModal', () => {
  let component: EditProfileModal;
  let fixture: ComponentFixture<EditProfileModal>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditProfileModal ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditProfileModal);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditProfileModal } from './edit-profile.modal';
import { ImageCropperModule } from 'ngx-image-cropper';
import { ImageUploadComponentModule } from '../../components/image-upload/image-upload.module';
import { LazyLoadImageModule } from 'ng-lazyload-image';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    ImageUploadComponentModule,
    ImageCropperModule,
    LazyLoadImageModule
  ],
  declarations: [EditProfileModal]
})
export class EditProfileModalModule { }

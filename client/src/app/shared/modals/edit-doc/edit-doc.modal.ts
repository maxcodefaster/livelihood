import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams, ToastController } from '@ionic/angular';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { PrivateDocService } from 'src/app/services/private-doc.service';
import { UserService } from '../../../core/user.service';
import { SharedDocService } from 'src/app/services/shared-doc.service';
import { ImgInterface } from '../../components/image-upload/img-interface';

@Component({
  selector: 'app-edit-doc',
  templateUrl: './edit-doc.modal.html',
  styleUrls: ['./edit-doc.modal.scss'],
})
export class EditDocModal implements OnInit {

  submitted = false;
  existingDoc: any = false;
  db: String;
  loading: any;
  form: FormGroup;
  modalTitel: String = 'Add post';
  selectedSegment = 'none';
  img: ImgInterface = {
    ratio: 16 / 9,
    maintainAspectRation: false,
    roundCropper: false,
    blob: undefined,
  };

  constructor(
    private modalCtrl: ModalController,
    private navParams: NavParams,
    private fb: FormBuilder,
    private privateDocService: PrivateDocService,
    private sharedDocService: SharedDocService,
    private userService: UserService,
    private toastCtrl: ToastController,
  ) {
    this.form = this.fb.group({
      title: new FormControl('', [
        Validators.required,
      ]),
      img: new FormControl('', []),
      note: new FormControl('', [
      ]),
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.form.controls; }

  ngOnInit() {
    if (typeof (this.navParams.get('doc')) !== 'undefined') {
      this.existingDoc = this.navParams.get('doc');
      this.form.patchValue({
        title: this.existingDoc.title,
        note: this.existingDoc.note
      })
      this.existingDoc.imgBlob ? this.img.blob = this.existingDoc.imgBlob : this.img.blob = new Blob();
      this.modalTitel = 'Edit post';
    } else {
      this.img.blob = new Blob();
    }
    if (typeof (this.navParams.get('db')) !== 'undefined') {
      this.db = this.navParams.get('db');
    }
  }

  segmentChanged(ev: any) {
    this.selectedSegment = ev.detail.value;
    if (this.selectedSegment === 'none') {
      this.form.patchValue({
        img: null
      });
    }
  }

  // save to Database
  async saveForm() {
    this.submitted = true;
    if (this.form.valid) {
      const form = this.form.value;
      const iso = this.getDateISOString();
      let res;
      let postData: any = {
        author: this.userService.currentUser.user_id,
        doc: this.existingDoc,
        title: form.title,
        note: form.note,
        file: form.img,
        dateCreated: iso,
        dateUpdated: iso
      }
      if (this.db === 'private') {
        // save to Private Database
        res = await this.privateDocService.savePrivateDocs(postData)
        res.ok ? this.close() : console.log(res);
      } else {
        // save to Shared Database
        res = await this.sharedDocService.saveSharedDocs(postData)
        res.ok ? this.close() : console.log(res);
      }
    } else {
      this.formErrorToast();
    }
  }

  getDateISOString(): string {
    return new Date().toISOString();
  }

  hasFormError(formField) {
    if (!this.f[formField].valid && this.submitted) {
      return true;
    }
    return false;
  }

  async formErrorToast() {
    const toast = await this.toastCtrl.create({
      header: 'Please fill in required form fields',
      duration: 2000,
      position: 'bottom',
      translucent: true,
      buttons: [
        {
          side: 'start',
          icon: 'warning-outline',
          handler: () => { }
        }
      ]
    });
    toast.present();
  }

  resetForm(form: FormGroup) {
    form.reset();
    this.submitted = false;
    Object.keys(form.controls).forEach(key => {
      form.get(key).setErrors(null);
    });
  }

  close(): void {
    this.modalCtrl.dismiss();
  }

}
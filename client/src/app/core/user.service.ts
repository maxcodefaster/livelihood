import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { SERVER_ADDRESS } from '../../environments/environment';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public currentUser: any = false;
  public profileImg;

  constructor(
    public storage: Storage,
    private http: HttpClient,
  ) { }

  saveUserData(data): void {
    this.currentUser = data;
    this.storage.set('userData', data);
  }

  getUserData(): Promise<any> {
    return this.storage.get('userData');
  }

  saveProfileImgLocal() {
    this.getProfileImgRemote(this.currentUser.user_id).subscribe((res) => {
      if (res) {
        this.profileImg = this.createImgObjectUrl(res);
        this.storage.set('userProfileImg', res);
      }
    }, (error) => {
      this.localImg2Blob().subscribe((res) => {
        this.profileImg = this.createImgObjectUrl(res);
        this.storage.set('userProfileImg', res);
      })
    });
  }

  createImgObjectUrl(blob) {
    let objectURL = URL.createObjectURL(blob);
    return objectURL;
  }

  async getProfileImgLocal(): Promise<any> {
    if (await this.storage.get('userProfileImg')) {
      let localImg = await this.storage.get('userProfileImg');
      let img = this.createImgObjectUrl(localImg)
      return img;
    }
  }

  getProfileImgRemote(id) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'responseType': 'blob',
      'Authorization': 'Bearer ' + this.currentUser.token + ':' + this.currentUser.password
    });
    return this.http.get(SERVER_ADDRESS + 'user/profile-img/' + id, { headers, responseType: 'blob' })
  }

  editUserProfile(formData) {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.currentUser.token + ':' + this.currentUser.password
    });
    return this.http.post(SERVER_ADDRESS + 'user/profile-img/', formData, { headers });
  }

  getUserProfile(id): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.currentUser.token + ':' + this.currentUser.password
    });
    return this.http.get(SERVER_ADDRESS + 'user/profile/' + id, { headers });
  }

  getUserDoc(id): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.currentUser.token + ':' + this.currentUser.password
    });
    return this.http.get(SERVER_ADDRESS + 'user/info/' + id, { headers });
  }

  editUser(newDetails) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.currentUser.token + ':' + this.currentUser.password
    });
    return this.http.post(SERVER_ADDRESS + 'user/admin/edit', newDetails, { headers });
  }

  delAcc(id) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.currentUser.token + ':' + this.currentUser.password
    });
    return this.http.get(SERVER_ADDRESS + 'user/delete/' + id, { headers });
  }

  getUserList() {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.currentUser.token + ':' + this.currentUser.password
    });
    return this.http.get(SERVER_ADDRESS + 'user/admin/list/', { headers });
  }

  deleteLocalStorage() {
    this.storage.clear();
  }

  localImg2Blob() {
    return this.http.get('/assets/core/user.png', { responseType: 'blob' })
  }

}

import { Injectable } from '@angular/core';
import { UserService } from './user.service';
import PouchDB from 'pouchdb-browser';

@Injectable({
    providedIn: 'root'
})
export class DataService {

    dbs = null;
    remoteAddress = [];
    remoteName = [];
    defaultSyncOpts = {
        live: true,
        retry: true,
    }

    constructor(
        private userService: UserService
    ) { }

    initDatabase(remote): void {
        this.remoteAddress = Object.values(remote.userDBs);
        this.remoteName = Object.keys(remote.userDBs);
        this.dbs = {};

        // save PouchDB instances and remote address
        for (let i = 0; i < this.remoteName.length; i++) {
            this.dbs[this.remoteName[i]] =
                new PouchDB(this.remoteName[i], {
                    auto_compaction: true
                });
            this.dbs[this.remoteName[i]].address = this.remoteAddress[i]
                .replace(/^http:/, 'https:')
                .replace(/@localhost:5984/, '@livelihood.foundation:6984');
        }
        this.initRemoteSync();
    }

    async initRemoteSync() {
        for (const db in this.dbs) {
            const dbRemote = this.dbs[db].address;
            this.dbs[db].sync(dbRemote, this.defaultSyncOpts)
            // .on('change', info => { console.log(info); }).on('active', () => { console.log('resume'); }).on('denied', err => { console.log('denied ' + err); })
            // .on('complete', info => { console.log('complete ' + JSON.stringify(info)); }).on('error', err => { console.log('error ' + err); });
        }
    }

    // Document Funtions
    fetchDoc(doc, dbname): Promise<any> {
        return this.dbs[dbname].get(doc);
    }

    createDoc(doc, dbname): Promise<any> {
        return this.dbs[dbname].post(doc);
    }

    updateDoc(doc, dbname): Promise<any> {
        return this.dbs[dbname].put(doc);
    }

    deleteDoc(doc, dbname): Promise<any> {
        return this.dbs[dbname].remove(doc);
    }

    // Attachment Functions
    addAttachment(id, fileName, rev, file, dbname): Promise<any> {
        return this.dbs[dbname].putAttachment(id, fileName, rev, file, file.type);
    }

    getAttachment(id, attId, dbname): Promise<any> {
        return this.dbs[dbname].getAttachment(id, attId);
    }

    rmAttachment(id, attId, rev, dbname): Promise<any> {
        return this.dbs[dbname].removeAttachment(id, attId, rev)
    }

    // Database Functions
    getDbInfo(dbname) {
        this.dbs[dbname] = new PouchDB(dbname, { auto_compaction: true });
        return this.dbs[dbname].info();
    }

    forceSync(dbname) {
        const remoteDB = 'https://' + this.userService.currentUser.token + ':' + this.userService.currentUser.password + '@livelihood.foundation:6984/' + dbname;
        PouchDB.sync(dbname, remoteDB, this.defaultSyncOpts);
    }

    async createDB(name, docs) {
        const remoteDB = 'https://' + this.userService.currentUser.token + ':' + this.userService.currentUser.password + '@livelihood.foundation:6984/' + name;
        this.dbs[name] = new PouchDB(name, { auto_compaction: true });
        for (const doc of docs) {
            this.createDoc(doc, name).then((res) => {
                // console.log(res)
            }).catch((err) => { console.log(err) });
        }
        PouchDB.sync(name, remoteDB, this.defaultSyncOpts);
        return new Promise((resolve, reject) => { resolve('db created'); });
    }

}

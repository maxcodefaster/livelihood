import { Injectable, RendererFactory2, Inject, Renderer2 } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  renderer: Renderer2;
  darkMode;

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private rendererFactory: RendererFactory2,
    public storage: Storage
  ) {
    this.renderer = this.rendererFactory.createRenderer(null, null);
  }

  enableDark() {
    this.renderer.addClass(this.document.body, 'dark');
    this.storage.set('darkMode', true);
    this.darkMode = true;
  }

  enableLight() {
    this.renderer.removeClass(this.document.body, 'dark');
    this.storage.set('darkMode', false);
    this.darkMode = false;
  }

  async getUserTheme() {
    if (await this.storage.get('darkMode') !== null) {
      this.darkMode = await this.storage.get('darkMode')
      this.darkMode ? this.renderer.addClass(this.document.body, 'dark') : this.renderer.removeClass(this.document.body, 'dark');
    }
  }

}
import { Injectable, NgZone } from '@angular/core';
import { DataService } from './data.service';
import { BehaviorSubject } from 'rxjs';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  // Define which database to access
  dbname: string = 'chat-index';
  chatSubject: BehaviorSubject<object[]> = new BehaviorSubject([]);
  chatListSubject: BehaviorSubject<object[]> = new BehaviorSubject([]);

  constructor(
    private dataService: DataService,
    private zone: NgZone,
    private userService: UserService,
  ) { }

  // create Chat Index Id for private conversations
  createPrivateChatId(userId) {
    let convoId = [];
    convoId.push(userId, this.userService.currentUser.user_id);
    convoId.sort();
    const convoName = convoId[0].toLowerCase() + convoId[1].toLowerCase();
    return convoName;
  }

  // individuell Chat
  async initConversation(contactName) {
    const convoName = this.createPrivateChatId(contactName);
    this.dataService.getDbInfo('chat$' + convoName).then((res) => {
      if (res.doc_count > 0) {
        this.emitMessages(convoName);
        this.dataService.dbs['chat$' + convoName].changes({ live: true, since: 'now', include_docs: true }).on('change', (change) => {
          if (change.doc.type === 'chatMsg' || change.deleted) {
            this.emitMessages(convoName);
          }
        });
        if (!Object.keys(this.userService.currentUser.userDBs).includes('chat$' + convoName)) {
          this.dataService.forceSync('chat$' + convoName);
        }
      } else {
        this.newPrivateConvo(contactName, convoName);
      }
    }).catch((err) => { console.log(err) });
  }

  getChat(): BehaviorSubject<object[]> {
    return this.chatSubject;
  }

  emitMessages(chatId): void {
    this.zone.run(() => {
      const options = {
        include_docs: true,
        descending: true
      };
      this.dataService.dbs['chat$' + chatId].query('chatMsg/by_date_created', options).then((data) => {
        const message = data.rows.map(row => {
          return row.doc;
        });
        message.reverse();
        this.chatSubject.next(message);
        if (!Object.keys(this.userService.currentUser.userDBs).includes('chat$' + chatId)) {
          this.dataService.forceSync('chat$' + chatId);
        }
      }).catch((err) => { console.log(err); });
    });
  }

  addMsg(msg): void {
    this.dataService.createDoc({
      chatDbId: msg.chatDbId,
      msg: msg.msg,
      author: msg.author,
      dateCreated: msg.dateCreated,
      type: 'chatMsg'
    }, msg.chatDbId);
  }

  // start a new private chat
  newPrivateConvo(userId, convoName) {
    const chatIndexDoc = {
      _id: 'chat$' + convoName,
      type: 'privateChat',
      participants: [userId, this.userService.currentUser.user_id],
    };
    const viewDocs = [{
      _id: '_design/chatMsg',
      views: {
        by_date_created: {
          map: "function(doc){ if(doc.type == 'chatMsg'){ emit(doc.dateCreated);} }"
        }
      }
    }];
    this.zone.run(() => {
      this.dataService.createDB('chat$' + convoName, viewDocs).then((res: any) => {
        this.dataService.createDoc(chatIndexDoc, this.dbname).then((res) => {
          // console.log(res)
        }).catch((err) => { console.log(err) })
        this.initConversation(userId);
      });
    })
  }

  // get chat List
  initChatList(): void {
    this.emitChatList();
    this.dataService.dbs[this.dbname].changes({ live: true, since: 'now', include_docs: true }).on('change', (change) => {
      if (change.doc.type === 'privateChat' || change.deleted) {
        this.emitChatList();
      }
    });
  }

  getChatList(): BehaviorSubject<object[]> {
    return this.chatListSubject;
  }

  forceChatListSync() {
    this.dataService.forceSync('chat-index');
  }

  emitChatList() {
    this.zone.run(() => {
      const opts = {
        include_docs: true,
        descending: true
      };
      this.dataService.dbs[this.dbname].query('chatIndex/privateChats', opts).then((chats) => {
        const message = chats.rows.map(row => {
          return row.doc;
        });
        this.chatListSubject.next(message);
      }).catch((err) => { console.log(err); });
    });
  }

}

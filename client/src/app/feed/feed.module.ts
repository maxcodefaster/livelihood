import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FeedPage } from './feed.page';
import { SkeletonCardModule } from '../shared/components/skeleton-card/skeleton-card.module';
import { DocListModule } from '../shared/components/doc-list/doc-list.module';
import { EditDocModalModule } from '../shared/modals/edit-doc/edit-doc.module';
import { LazyLoadImageModule } from 'ng-lazyload-image';

const routes: Routes = [
  {
    path: '',
    component: FeedPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    EditDocModalModule,
    DocListModule,
    SkeletonCardModule,
    RouterModule.forChild(routes),
    LazyLoadImageModule
  ],
  declarations: [FeedPage]
})
export class FeedPageModule { }

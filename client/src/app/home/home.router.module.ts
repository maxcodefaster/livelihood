import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomePage } from './home.page';

const routes: Routes = [
    {
        path: '',
        component: HomePage,
        children: [
            {
                path: 'conversations',
                children: [
                    {
                        path: '',
                        loadChildren: () => import('../chat/conversations/conversations.module').then(m => m.ConversationsPageModule)
                    }
                ]
            },
            {
                path: 'feed',
                children: [
                    {
                        path: '',
                        loadChildren: () => import('../feed/feed.module').then(m => m.FeedPageModule)
                    }
                ]
            },
            {
                path: 'me',
                children: [
                    {
                        path: '',
                        loadChildren: () => import('../profile/me/me.module').then(m => m.MePageModule)
                    }
                ]
            }
        ]
    },
    {
        path: '',
        redirectTo: '/app/feed',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class HomePageRoutingModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ProfilePage } from './profile.page';
import { SkeletonCardModule } from '../../shared/components/skeleton-card/skeleton-card.module';
import { DocListModule } from '../../shared/components/doc-list/doc-list.module';
import { EditDocModalModule } from '../../shared/modals/edit-doc/edit-doc.module';
import { LazyLoadImageModule } from 'ng-lazyload-image';

const routes: Routes = [
  {
    path: '',
    component: ProfilePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    DocListModule,
    EditDocModalModule,
    SkeletonCardModule,
    RouterModule.forChild(routes),
    LazyLoadImageModule
  ],
  declarations: [ProfilePage]
})
export class ProfilePageModule {}

import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { IonContent, IonList, LoadingController, NavController } from '@ionic/angular';
import { AuthService } from 'src/app/core/auth.service';
import { UserService } from 'src/app/core/user.service';

@Component({
  selector: 'app-ride',
  templateUrl: './ride.page.html',
  styleUrls: ['./ride.page.scss'],
})
export class RidePage implements OnInit {

  @ViewChild(IonContent) contentArea: IonContent;
  @ViewChild(IonList, { read: ElementRef }) chatList: ElementRef;

  chatMsgs: Object[] = [];
  loading: any;
  currentUser;
  mutationObserver: any;

  pickMeUp: boolean = false;
  alone: boolean = false;
  share: boolean = false;

  constructor(
    private loadingCtrl: LoadingController,
    private authService: AuthService,
    public userService: UserService,
    private navCtrl: NavController,
  ) { }

  ngOnInit() {
    this.loadingCtrl.create({
      translucent: true,
      message: 'Authenticating...'
    }).then((overlay) => {
      this.loading = overlay;
      this.loading.present();
      this.authService.reauthenticate().then((res) => {
        this.currentUser = this.userService.currentUser;
        this.addChat('bot', 'Do you want me to pick you up?')
        this.loading.dismiss();
      }, (err) => {
        this.loading.dismiss();
        this.navCtrl.navigateRoot('/login');
      });
    });
  }

  ngAfterViewInit() {
    this.mutationObserver = new MutationObserver((mutations) => {
      this.newChatAdded();
    });
    this.mutationObserver.observe(this.chatList.nativeElement, {
      childList: true
    });
  }

  newChatAdded(): void {
    this.contentArea.scrollToBottom();
  }

  async pickMeUpInit() {
    this.pickMeUp = true;
    this.addChat(this.currentUser.user_id, 'Yes, pick me up!')
    await new Promise(resolve => setTimeout(resolve, 1000));
    this.addChat('bot', 'OK, do you want to drive alone or share your ride and save same money?')
  }

  async driveAlone() {
    this.alone = true;
    this.addChat(this.currentUser.user_id, 'I would rather drive alone')
    await new Promise(resolve => setTimeout(resolve, 1000));
    this.addChat('bot', 'Sure! I will be there in 15 min.')
    await new Promise(resolve => setTimeout(resolve, 1000));
    this.finalMessage();
  }

  async shareRide() {
    this.share = true;
    this.addChat(this.currentUser.user_id, 'Sharing is fine with me!')
    await new Promise(resolve => setTimeout(resolve, 1000));
    this.addChat('bot', 'Perfect! I will be there in 15 min.')
    await new Promise(resolve => setTimeout(resolve, 1000));
    this.finalMessage();
  }

  finalMessage() {
    this.addChat('bot', 'I hope you enjoyed this quick demo. Besides the carsharing and the utility bot, the whole application is fully functional. Please check out the CAIN-Stack')
  }

  addChat(author, msg): void {
    let iso = this.getDateISOString();
    this.chatMsgs.push({
      author: author,
      msg: msg,
      dateCreated: iso,
    });
  }

  getDateISOString(): string {
    return new Date().toISOString();
  }

}

import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { IonContent, IonList, LoadingController, NavController } from '@ionic/angular';
import { AuthService } from 'src/app/core/auth.service';
import { UserService } from 'src/app/core/user.service';

@Component({
  selector: 'app-utility',
  templateUrl: './utility.page.html',
  styleUrls: ['./utility.page.scss'],
})
export class UtilityPage implements OnInit {

  @ViewChild(IonContent) contentArea: IonContent;
  @ViewChild(IonList, { read: ElementRef }) chatList: ElementRef;

  chatMsgs: Object[] = [];
  loading: any;
  currentUser;
  mutationObserver: any;

  inetSpeed: number = 50;
  powerUsage: number = 50;
  peers: number = 5;

  stopSharing: boolean = false;


  constructor(
    private loadingCtrl: LoadingController,
    private authService: AuthService,
    public userService: UserService,
    private navCtrl: NavController,
  ) { }

  ngOnInit() {
    this.loadingCtrl.create({
      translucent: true,
      message: 'Authenticating...'
    }).then((overlay) => {
      this.loading = overlay;
      this.loading.present();
      this.authService.reauthenticate().then((res) => {
        this.currentUser = this.userService.currentUser;
        this.updateMB();
        this.updatePowerUsage();
        this.updatePeers();
        this.loading.dismiss();
      }, (err) => {
        this.loading.dismiss();
        this.navCtrl.navigateRoot('/login');
      });
    });
  }

  ngAfterViewInit() {
    this.mutationObserver = new MutationObserver((mutations) => {
      this.newChatAdded();
    });
    this.mutationObserver.observe(this.chatList.nativeElement, {
      childList: true
    });
  }

  newChatAdded(): void {
    this.contentArea.scrollToBottom();
  }

  async updateMB() {
    while (true) {
      await new Promise(resolve => setTimeout(resolve, 1000));
      this.inetSpeed = Math.floor(Math.random() * 100) + 50;
    }
  }

  async updatePowerUsage() {
    while (true) {
      await new Promise(resolve => setTimeout(resolve, 1000));
      this.powerUsage = Math.floor(Math.random() * 100) + 250;
    }
  }

  async updatePeers() {
    while (true) {
      await new Promise(resolve => setTimeout(resolve, 3000));
      this.peers = Math.floor(Math.random() * 10);
    }
  }

  async stopSharingInit() {
    this.stopSharing = true;
    this.addChat(this.currentUser.user_id, 'Stop sharing')
    await new Promise(resolve => setTimeout(resolve, 1000));
    this.addChat('bot', 'STOP SHARING???')
    await new Promise(resolve => setTimeout(resolve, 1000));
    this.addChat('bot', 'YOU ARE NOT PART OF THIS SHARING ECONOMY!')
    await new Promise(resolve => setTimeout(resolve, 1000));
    this.addChat('bot', 'You will be logged out in 5...')
    await new Promise(resolve => setTimeout(resolve, 1000));
    this.addChat('bot', '4...')
    await new Promise(resolve => setTimeout(resolve, 1000));
    this.addChat('bot', '3...')
    await new Promise(resolve => setTimeout(resolve, 1000));
    this.addChat('bot', '2...')
    await new Promise(resolve => setTimeout(resolve, 1000));
    this.addChat('bot', '1...')
    await new Promise(resolve => setTimeout(resolve, 1000));
    this.addChat('bot', 'BYE BYE!')
    await new Promise(resolve => setTimeout(resolve, 1000));
    this.authService.logout();
  }

  finalMessage() {
    this.addChat('bot', 'I hope you enjoyed this quick demo. Besides the carsharing and the utility bot, the whole application is fully functional. Please check out the CAIN-Stack')
  }

  addChat(author, msg): void {
    let iso = this.getDateISOString();
    this.chatMsgs.push({
      author: author,
      msg: msg,
      dateCreated: iso,
    });
  }

  getDateISOString(): string {
    return new Date().toISOString();
  }


}

import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ThemeService } from './core/theme.service';
import { Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private metaService: Meta,
    private theme: ThemeService,
  ) {
    const prefersDark = window.matchMedia('(prefers-color-scheme: dark)');
    prefersDark.matches ? this.theme.enableDark() : this.theme.getUserTheme();
    this.initializeApp();
  }

  ngOnInit(): void {
    this.metaService.addTags([
      { name: 'keywords', content: 'livelihood.foundation, COCO, Code & Context, TH Köln, Max Heichling' },
      { name: 'description', content: 'A sustainable & affordable alternative to get your livelihoods. A decentralised sharing economy, by the people, for the people.' },
      { name: 'robots', content: 'index, follow' }
    ]);
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

}

import { Component, OnInit } from '@angular/core';
import { AuthService } from '../core/auth.service';
import { NavController } from '@ionic/angular';
import { UserService } from '../core/user.service';

@Component({
  selector: 'app-info',
  templateUrl: './info.page.html',
  styleUrls: ['./info.page.scss'],
})
export class InfoPage implements OnInit {

  currentUser;

  constructor(
    private navCtrl: NavController,
    private authService: AuthService,
    public userService: UserService
  ) { }

  ngOnInit() {
    this.authService.reauthenticate().then((res) => {
      this.currentUser = this.userService.currentUser;
    }, (err) => {
    });
  }

}
